def app():
    print(get_multiples_of_n_below_x(3, 1000) + get_multiples_of_n_below_x(5, 1000))


def get_multiples_of_n_below_x(n: int, x: int) -> int:
    return sum([i for i in range(1, x) if i % n == 0])


if __name__ == '__main__':
    app()
